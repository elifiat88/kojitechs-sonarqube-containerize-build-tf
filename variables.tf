

variable "component_name" {
  description = "component name"
  type        = string
  default     = "sonarqube-server"
}

variable "container_port" {
  description = "sonarQube port"
  type        = number
  default     = 9000
}


variable "database_name" {
  description = "sonarQube database name"
  type        = string
  default     = "sonar"
}

variable "master_username" {
  description = "sonarQube database master user name"
  type        = string
  default     = "sonar"
}