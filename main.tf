

terraform {
  required_version = ">=1.1.5"

  backend "s3" {
    bucket         = "sonarqube-tf"
    dynamodb_table = "sonarqube-tf"
    key            = "path/env"
    region         = "us-east-1"
    encrypt        = "true"
  }
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
}

provider "aws" {
  region = "us-east-1"
}

locals {
    azs =  data.aws_availability_zones.available.names 
    vpc_id = module.vpc.vpc_id
    public_subnet =  module.vpc.public_subnets
    private_subnet =  module.vpc.private_subnets
    name              = "kojitechs-${replace(basename(var.component_name), "_", "-")}"
    db_subnets_names = module.vpc.database_subnet_group_name
}

data "aws_availability_zones" "available" {
  state = "available"
}

module "vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name = "${var.component_name}-vpc" 
  cidr = "10.0.0.0/16"

  azs             = local.azs
  private_subnets = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
  public_subnets  = ["10.0.101.0/24", "10.0.102.0/24", "10.0.103.0/24"]

  enable_nat_gateway = true
  enable_vpn_gateway = true

  tags = {
    Terraform = "true"
  }
}


module "aurora" {
   source = "git::https://github.com/Bkoji1150/aws-rdscluster-kojitechs-tf.git?ref=v1.1.11"

  component_name = var.component_name
  name           = local.name
  engine         = "aurora-postgresql"
  engine_version = "11.15"
  instances = {
    1 = {
      instance_class      = "db.r5.2xlarge"
      publicly_accessible = false
    }
  }

  vpc_id                 = local.vpc_id 
  create_db_subnet_group = true
  subnets                = local.private_subnet

  create_security_group = true
  vpc_security_group_ids =  [aws_security_group.postgres-sg.id]
  iam_database_authentication_enabled = true

  apply_immediately   = true
  skip_final_snapshot = true

  enabled_cloudwatch_logs_exports = ["postgresql"]
  database_name                   = var.database_name
  master_username                 = var.master_username
}

